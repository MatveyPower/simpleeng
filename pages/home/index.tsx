import { Component } from 'nuxt-property-decorator'
import Header from '~/components/header/header'
import { VueComponent } from '~/types/vue-ts-component'
import styles from '../home/index.css?module'
@Component
export default class HomePage extends VueComponent {
  render() {
    return <Header a='s'/>
  }
}
