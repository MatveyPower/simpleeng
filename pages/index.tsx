import { Component } from 'nuxt-property-decorator'
import { VueComponent } from '~/types/vue-ts-component'

@Component({
  middleware({
    redirect,
  }) {
    redirect('/home')
  }
})
export default class MainPage extends VueComponent {
}
