import Vue from 'vue'

export type CSSClass = (string | false | {
    [key: string]: boolean
})

export class VueComponent<P = {}> extends Vue {
    // @ts-expect-error
    public $props: P & {
        key?: string,
        classs?: CSSClass | CSSClass[]
    }
}